# terraform-modules

reusable terraform modules for DRY

## Modules

### fargate-service

create fargate-services out of our docker images

#### example

start an nginx under https://my-nginx.seatris.io

```
module "fargate-service" {
  source         = "git::https://gitlab.com/seatris-public/terraform-modules.git//fargate-service?ref=v1.0"
  service_name   = "my-nginx"
  tags           = {}
  container_port = 80
  health_check   = "/"

  service_image = "nginx:latest"

  task_environment_vars = [
    {
      name  = "ENV"
      value = "${terraform.workspace}"
    },
  ]

  task_environment_secrets = [
    {
      name      = "DATABASE_URL"
      # secret parameters have to be defined under the name /${terraform.workspace}/${service_name}/
      valueFrom = "/${terraform.workspace}/testme/database_url"
    }
  ]
}

```
