# to retrieve account-id:
data "aws_caller_identity" "current" {}
data "aws_region" "current" {}
data "aws_vpc" "main" {
  tags = {
    Name = "main-${terraform.workspace}"
  }
}
data "aws_subnet_ids" "private_subnets" {
  vpc_id = data.aws_vpc.main.id
  tags = {
    Type = "private-${terraform.workspace}"
  }
}
data "aws_security_groups" "task_security_group" {
  filter {
    name = "group-name"
    values = [
      "services-${terraform.workspace}-task"]
  }
  filter {
    name = "vpc-id"
    values = [
      data.aws_vpc.main.id]
  }
}
locals {
  # does the lambda need access to the VPC?
  security_group_ids = var.vpc_access == 1 ? data.aws_security_groups.task_security_group.ids : []
  subnet_ids = var.vpc_access == 1 ? data.aws_subnet_ids.private_subnets.ids : []
  default_config = map("REGION", data.aws_region.current.name, "environment", terraform.workspace)
  # add the env vars that the caller of the module handed over:
  mergedConfig = merge(var.additional_environment_params, local.default_config)
}

resource "aws_lambda_function" "lambda-service" {
  function_name = var.lambda_name
  handler = var.lambda_handler
  role = aws_iam_role.iam_for_lambda.arn
  runtime = "provided"
  filename = var.lambda_zipfile
  source_code_hash = filebase64sha256(var.lambda_zipfile)
  memory_size = "2048"
  timeout = 20
  reserved_concurrent_executions = 30
  publish = true

  environment {
    variables = local.mergedConfig
  }
  vpc_config {
    security_group_ids = local.security_group_ids
    subnet_ids = local.subnet_ids
  }
}

resource "aws_lambda_alias" "lambda_alias" {
  name             = "prod"
  function_name    = aws_lambda_function.lambda-service.function_name
  function_version = aws_lambda_function.lambda-service.version
}

resource "aws_iam_role" "iam_for_lambda" {
  name = "role-${var.lambda_name}"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

data "aws_iam_policy_document" "lambda_policy" {
  statement {
    effect = "Allow"
    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents"
    ]
    resources = [
      "*"]
  }
  statement {
    effect = "Allow"
    actions = [
      "ec2:CreateNetworkInterface",
      "ec2:DescribeNetworkInterfaces",
      "ec2:DeleteNetworkInterface"]
    resources = [
      "*"]
  }
}

resource "aws_iam_role_policy" "lambda_policy" {
  name = "policy-${var.lambda_name}"
  role = aws_iam_role.iam_for_lambda.id

  policy = data.aws_iam_policy_document.lambda_policy.json
}

output "function_arn" {
  value = aws_lambda_function.lambda-service.arn
}
