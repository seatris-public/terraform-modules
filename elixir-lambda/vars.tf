variable "lambda_name" {
  type = string
  description = "The name of the lambda that listens for messages"
}
variable "lambda_handler" {
  type = string
  description = "The handler of the lambda that listens for messages"
}
variable "lambda_zipfile" {
  type = string
  description = "The path to the zipped lambda artifact"
}

variable "additional_environment_params" {
  type = map(string)
  default = {}
}
variable "vpc_access" {
  type = number
  default = 0
}
