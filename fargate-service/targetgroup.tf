locals {
  subdomain = "${var.dns_name}${terraform.workspace == "live" ? "" : format("-%s", terraform.workspace)}"
}

resource "aws_alb_target_group" "service" {
  count = var.use_alb ? 1 : 0
  name                 = "${var.service_name}-${terraform.workspace}"
  port                 = "80"
  protocol             = "HTTP"
  vpc_id               = data.aws_vpc.vpc.id
  target_type          = "ip"
  deregistration_delay = "30"

  health_check {
    path                = var.health_check
    matcher             = "200"
    interval            = "30"
    timeout             = "10"
    healthy_threshold   = 5
    unhealthy_threshold = 5
  }

  tags = var.tags

  # the targetgroup can only be replaced if a new one is created first
  # otherwise the association to listener rule prevents deletion of targetgroup
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_lb_listener_rule" "service_listener_rule" {
  count = var.use_alb ? 1 : 0
  listener_arn = data.aws_lb_listener.https_listener.arn

  action {
    type             = "forward"
    target_group_arn = aws_alb_target_group.service[0].arn
  }

  condition {
    host_header {
      values = ["${local.subdomain}.*"]
    }
  }
}

data "aws_route53_zone" "domain_seatris_io" {
  name = "${terraform.workspace == "live" ? "" : "dev."}seatris.io."
  private_zone = false
}

# make the entry on the seatris.io zone (seatris.com is still on UD)
resource "aws_route53_record" "service_subdomain_seatris_io" {
  count = var.use_alb ? 1 : 0
  zone_id = data.aws_route53_zone.domain_seatris_io.zone_id
  name = local.subdomain
  type = "CNAME"
  ttl = "300"
  records = [
    data.aws_lb.alb.dns_name]
}
