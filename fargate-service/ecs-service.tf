locals {
  fluentbit_loggroup = "/fargate/fluent-bit/${var.service_name}-${terraform.workspace}"
  container_name = "${var.service_name}-${terraform.workspace}"
}


resource "aws_appautoscaling_target" "app_scale_target" {
  service_namespace  = "ecs"
  resource_id        = "service/${data.aws_ecs_cluster.fargate_cluster.cluster_name}/${aws_ecs_service.app.name}"
  scalable_dimension = "ecs:service:DesiredCount"
  max_capacity       = var.ecs_autoscale_max_instances
  min_capacity       = var.ecs_autoscale_min_instances
}

resource "aws_appautoscaling_policy" "autoscale_up" {
  name = "application-scaling"
  service_namespace = aws_appautoscaling_target.app_scale_target.service_namespace
  resource_id = aws_appautoscaling_target.app_scale_target.resource_id
  scalable_dimension = aws_appautoscaling_target.app_scale_target.scalable_dimension
  policy_type = "TargetTrackingScaling"

  target_tracking_scaling_policy_configuration {
    customized_metric_specification {
      metric_name = "CPUUtilization"
      namespace = "AWS/ECS"
      statistic = "Average"
      dimensions {
        name = "ClusterName"
        value = data.aws_ecs_cluster.fargate_cluster.cluster_name
      }
      dimensions {
        name = "ServiceName"
        value = aws_ecs_service.app.name
      }
    }

    scale_in_cooldown = 300
    scale_out_cooldown = 60
    target_value = var.scaling_target_cpu
  }
}

resource "aws_ecs_task_definition" "app" {
  family = "${var.service_name}-${terraform.workspace}"

  requires_compatibilities = [
    "FARGATE",
  ]

  network_mode       = "awsvpc"
  cpu                = var.cpu
  memory             = var.memory
  execution_role_arn = aws_iam_role.ecsTaskExecutionRole.arn

  # defined in role.tf
  task_role_arn = aws_iam_role.app_role.arn

  container_definitions = <<DEFINITION
[
  {
    "name": "${local.container_name}",
    "image": "${var.service_image}",
    "essential": true,
    "cpu": ${var.cpu},
    "portMappings": [
      {
        "protocol": "tcp",
        "containerPort": ${var.container_port},
        "hostPort": ${var.container_port}
      }
    ],
    "environment": ${jsonencode(var.task_environment_vars)},
    "secrets": ${jsonencode(var.task_environment_secrets)},
    "logConfiguration": {
      "logDriver": "awsfirelens",
      "options": {
        "Name": "newrelic",
        "endpoint": "https://log-api.eu.newrelic.com/log/v1"
      },
      "secretOptions": [{
        "name": "apiKey",
        "valueFrom": "${data.aws_ssm_parameter.newrelic_key.arn}"
      }]
    },
    "linuxParameters": {
      "initProcessEnabled": true
    }
  },
  {
    "essential": true,
    "image": "533243300146.dkr.ecr.eu-central-1.amazonaws.com/newrelic/logging-firelens-fluentbit",
    "name": "log_router",
    "logConfiguration": {
      "logDriver": "awslogs",
      "options": {
        "awslogs-group": "${local.fluentbit_loggroup}",
        "awslogs-region": "eu-central-1",
        "awslogs-stream-prefix": "fluent"
      }
    },
    "firelensConfiguration": {
        "type": "fluentbit",
        "options": {
            "enable-ecs-log-metadata": "true"
        }
    }
 }
]
DEFINITION
}

resource "aws_ecs_service" "app" {
  name            = "${var.service_name}-${terraform.workspace}"
  cluster         = data.aws_ecs_cluster.fargate_cluster.id
  launch_type     = "FARGATE"
  platform_version = "1.4.0"
  enable_execute_command = true
  task_definition = aws_ecs_task_definition.app.arn
  desired_count   = var.instances

  network_configuration {
    security_groups = [
      data.aws_security_group.task_nsg.id,
    ]

    subnets = data.aws_subnet_ids.private_subnets.ids
  }

  dynamic "load_balancer" {
    for_each = var.use_alb ? [1] : []
    content {
      target_group_arn = aws_alb_target_group.service[0].id
      container_name   = local.container_name
      container_port   = var.container_port
    }
  }
  dynamic "service_registries" {
    for_each = var.use_alb ? [] : [1]
    content {
      registry_arn = var.service_discovery_service_arn
      port = var.container_port
    }
  }
  //  service_registries {
  //    registry_arn = "${aws_service_discovery_service.fargate.arn}"
  //    port = "${var.container_port}"
  //  }
}

resource "aws_cloudwatch_log_group" "task_log_group" {
  name = local.fluentbit_loggroup
  retention_in_days = "14"
  tags              = var.tags
}

// TODO: remove this after our Data is in Newrelic
resource "aws_cloudwatch_log_group" "logs" {
  name              = "/fargate/service/${var.service_name}-${terraform.workspace}"
  retention_in_days = "14"
  tags              = var.tags
}

