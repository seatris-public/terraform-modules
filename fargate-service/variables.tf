# the service_name will be used as prefix to all resources created for the service
variable "service_name" {}

# the dns_name will be used as the subdomain to "seatris.io"
variable "dns_name" {}

# How many containers to run
variable "instances" {
  default = "1"
}

variable "ecs_autoscale_min_instances" {
  default = "1"
}

variable "ecs_autoscale_max_instances" {
  default = "2"
}

variable "memory" {
  default = "1024"
}

variable "cpu" {
  default = "512"
}
variable "scaling_target_cpu" {
  default = "60"
}

variable "container_port" {}

# The path to the health check for the load balancer to know if the container(s) are ready
variable "health_check" {}

variable "tags" {
  type = map(string)
}

# The default docker image to deploy with the infrastructure.
variable "service_image" {}

# list of name-value pairs to go into environment of task definition
variable "task_environment_vars" {
  type = list(map(string))
}

# list of name-valueFrom pairs to go into environment of task definition
variable "task_environment_secrets" {
  type    = list(map(string))
  default = []
}

variable "target_400_threshold" {
  default = "10"
}
variable "target_500_threshold" {
  default = "5"
}

// additional policies to add to the app:
variable "additional_policy_json"{
  type = string
  default = ""
  description = "policy-json string to add additional permissions to the app's role"
}

variable "use_alb" {
  type = bool
  default = true
  description = "should we register at api loadbalancer"
}

variable "http_api_id" {
  type = string
  default = ""
  description = "if you http api instead of loadbalancer"
}

variable "service_discovery_service_arn" {
  type = string
  default = ""
  description = "if you want to register at service discovery instead of loadbalancer"
}

output "target_group_arn" {
  value = var.use_alb ? aws_alb_target_group.service[0].arn : ""
}
