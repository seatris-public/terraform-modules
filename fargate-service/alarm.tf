data "aws_sns_topic" "alarm" {
  name = "alarms-topic-${terraform.workspace}"
}

resource "aws_cloudwatch_metric_alarm" "unhealthy" {
  count = var.use_alb ? 1 : 0
  alarm_name                = "${var.service_name}-unhealthyhosts-${terraform.workspace}"
  evaluation_periods        = "2"
  period                    = "60"
  metric_name               = "UnHealthyHostCount"
  namespace                 = "AWS/ApplicationELB"
  dimensions = {
    TargetGroup = aws_alb_target_group.service[0].arn_suffix
    LoadBalancer = data.aws_lb.alb.arn_suffix
  }
  statistic                 = "Sum"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  threshold                 = "1"
  alarm_description         = "This metric monitors monitors unhealthy targets"
  alarm_actions             = [ data.aws_sns_topic.alarm.arn ]
  ok_actions                = [ data.aws_sns_topic.alarm.arn ]
  insufficient_data_actions = [ data.aws_sns_topic.alarm.arn ]
}

resource "aws_cloudwatch_metric_alarm" "target_500" {
  count = var.use_alb ? 1 : 0
  alarm_name                = "${var.service_name}-tg-500-${terraform.workspace}"
  evaluation_periods        = "2"
  period                    = "60"
  metric_name               = "HTTPCode_Target_5XX_Count"
  namespace                 = "AWS/ApplicationELB"
  dimensions = {
    TargetGroup = aws_alb_target_group.service[0].arn_suffix
    LoadBalancer = data.aws_lb.alb.arn_suffix
  }
  statistic                 = "Sum"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  threshold                 = var.target_500_threshold
  alarm_description         = "This metric monitors target 500 errors"
  alarm_actions             = [ data.aws_sns_topic.alarm.arn ]
  ok_actions                = [ data.aws_sns_topic.alarm.arn ]
  insufficient_data_actions = [ data.aws_sns_topic.alarm.arn ]
}

resource "aws_cloudwatch_metric_alarm" "target_400" {
  count = var.use_alb ? 1 : 0
  alarm_name                = "${var.service_name}-tg-400-${terraform.workspace}"
  evaluation_periods        = "2"
  period                    = "60"
  metric_name               = "HTTPCode_Target_4XX_Count"
  namespace                 = "AWS/ApplicationELB"
  dimensions = {
    TargetGroup = aws_alb_target_group.service[0].arn_suffix
    LoadBalancer = data.aws_lb.alb.arn_suffix
  }
  statistic                 = "Sum"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  threshold                 = var.target_400_threshold
  alarm_description         = "This metric monitors target 400 errors"
  alarm_actions             = [ data.aws_sns_topic.alarm.arn ]
  ok_actions                = [ data.aws_sns_topic.alarm.arn ]
  insufficient_data_actions = [ data.aws_sns_topic.alarm.arn ]
}

resource "aws_cloudwatch_metric_alarm" "target__api_500" {
  count = var.http_api_id != "" ? 1 : 0
  alarm_name                = "${var.service_name}-tg-500-${terraform.workspace}"
  evaluation_periods        = "2"
  period                    = "60"
  metric_name               = "5xx"
  namespace                 = "AWS/ApiGateway"
  dimensions = {
    ApiId = var.http_api_id
  }
  statistic                 = "Sum"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  threshold                 = var.target_500_threshold
  alarm_description         = "This metric monitors target 500 errors"
  alarm_actions             = [ data.aws_sns_topic.alarm.arn ]
  ok_actions                = [ data.aws_sns_topic.alarm.arn ]
  insufficient_data_actions = [ data.aws_sns_topic.alarm.arn ]
}

resource "aws_cloudwatch_metric_alarm" "target_api_400" {
  count = var.http_api_id != "" ? 1 : 0
  alarm_name                = "${var.service_name}-tg-400-${terraform.workspace}"
  evaluation_periods        = "2"
  period                    = "60"
  metric_name               = "4xx"
  namespace                 = "AWS/ApiGateway"
  dimensions = {
    ApiId = var.http_api_id
  }
  statistic                 = "Sum"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  threshold                 = var.target_400_threshold
  alarm_description         = "This metric monitors target 400 errors"
  alarm_actions             = [ data.aws_sns_topic.alarm.arn ]
  ok_actions                = [ data.aws_sns_topic.alarm.arn ]
  insufficient_data_actions = [ data.aws_sns_topic.alarm.arn ]
}
