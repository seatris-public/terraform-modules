data "aws_caller_identity" "current" {}
data "aws_ssm_parameter" "newrelic_key" {
  name = "/account/newrelic/apikey"
}

locals {
  # this is a dirty hack: for the ECS service "api" all secrets are historically under "restaurantmanager"
  # we can remove that when we get rid of the cron instances (and rename the secret paths to ".../api/...")
  secretsManagerServiceName = var.service_name == "api" ? "restaurantmanager" : var.service_name
}

# creates an application role that the container/task runs as
resource "aws_iam_role" "app_role" {
  name               = "${var.service_name}-${terraform.workspace}"
  assume_role_policy = data.aws_iam_policy_document.app_role_assume_role_policy.json
}

data "aws_iam_policy_document" "app_role_assume_role_policy" {
  statement {
    actions = ["sts:AssumeRole"]
    principals {
      type        = "Service"
      identifiers = ["ecs-tasks.amazonaws.com"]
    }
  }
}
data "aws_iam_policy_document" "ssm_app_policy" {
  statement {
    actions = [
      "ssmmessages:CreateControlChannel",
      "ssmmessages:CreateDataChannel",
      "ssmmessages:OpenControlChannel",
      "ssmmessages:OpenDataChannel"
    ]
    resources = ["*"]
  }
}
resource "aws_iam_role_policy" "app_ssm_policy" {
  name   = "${var.service_name}-ssm-${terraform.workspace}"
  role   = aws_iam_role.app_role.id
  policy = data.aws_iam_policy_document.ssm_app_policy.json
}
# assigns additional app policy
resource "aws_iam_role_policy" "additional_app_policy" {
  count = var.additional_policy_json == "" ? 0 : 1
  name   = "${var.service_name}-additional-${terraform.workspace}"
  role   = aws_iam_role.app_role.id
  policy = var.additional_policy_json
}

#################################################################
#################################################################
# And here is the Role for the ecs execution:
# https://docs.aws.amazon.com/AmazonECS/latest/developerguide/task_execution_IAM_role.html
resource "aws_iam_role" "ecsTaskExecutionRole" {
  name               = "${var.service_name}-${terraform.workspace}-ecs"
  assume_role_policy = data.aws_iam_policy_document.assume_role_policy.json
}

data "aws_iam_policy_document" "assume_role_policy" {
  statement {
    actions = [
      "sts:AssumeRole",
    ]
    principals {
      type = "Service"
      identifiers = [
        "ecs-tasks.amazonaws.com",
      ]
    }
  }
}

# assigns the app policy
resource "aws_iam_role_policy" "ecsTask_policy" {
  role   = aws_iam_role.ecsTaskExecutionRole.id
  name   = "${var.service_name}-${terraform.workspace}-ecs"
  policy = data.aws_iam_policy_document.ssm_and_kms_policy.json
}

resource "aws_iam_role_policy_attachment" "ecsTaskExecutionRole_policy" {
  role       = aws_iam_role.ecsTaskExecutionRole.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
}

data "aws_iam_policy_document" "ssm_and_kms_policy" {
  statement {
    actions = [
      "ecs:DescribeClusters",
    ]
    resources = [
      data.aws_ecs_cluster.fargate_cluster.arn,
    ]
  }
  statement {
    actions = [
      "ssm:DescribeParameters",
    ]

    resources = ["*"]
  }
  statement {
    actions = [
      "ssm:GetParameters",
    ]
    resources = [
      "arn:aws:ssm:eu-central-1:${data.aws_caller_identity.current.account_id}:parameter/${terraform.workspace}/${var.service_name}/*",
      data.aws_ssm_parameter.newrelic_key.arn
    ]
  }

  statement {
    actions = ["secretsmanager:GetSecretValue", "secretsmanager:DescribeSecret"]
    resources = ["arn:aws:secretsmanager:eu-central-1:${data.aws_caller_identity.current.account_id}:secret:/${terraform.workspace}/${local.secretsManagerServiceName}/*"]
  }

  statement {
    actions   = ["kms:Decrypt"]
    resources = [data.aws_kms_alias.service_alias.target_key_arn]
  }
}

