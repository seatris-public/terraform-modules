data "aws_ecs_cluster" "fargate_cluster" {
  cluster_name = "fargate-${terraform.workspace}"
}

data "aws_security_group" "task_nsg" {
  name = "services-${terraform.workspace}-task"
}

data "aws_vpc" "vpc" {
  tags = {
    Name = "main-${terraform.workspace}"
  }
}

data "aws_subnet_ids" "private_subnets" {
  vpc_id = data.aws_vpc.vpc.id
  tags = {
    Type = "private-${terraform.workspace}"
  }
}

data "aws_kms_alias" "service_alias" {
  name = "alias/${terraform.workspace}/service-secrets"
}

data "aws_lb" "alb" {
  name = "api-${terraform.workspace}"
}

data "aws_lb_listener" "https_listener" {
  load_balancer_arn = data.aws_lb.alb.arn
  port = 443
}
